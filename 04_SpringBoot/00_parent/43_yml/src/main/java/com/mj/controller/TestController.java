package com.mj.controller;

import com.mj.domain.Cat;
import com.mj.domain.Person;
import com.mj.domain.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Method;

@RestController
@EnableConfigurationProperties({Student.class, Cat.class})
public class TestController {
    @Autowired
    private Cat cat;

    @GetMapping("/test")
    public String test() {
        System.out.println(cat);
        return "test!!!";
    }
}
