package com.mj;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.helpers.LogLog;

public class TestJCL_Log4j1 {
    public static void main(String[] args) {
        // LogLog.setInternalDebugging(true);

        Log log = LogFactory.getLog(TestJCL_Log4j1.class);
        log.info("信息_INFO");
        log.error("错误_ERROR");
        log.debug("调试_DEBUG");
        log.trace("痕迹_TRACE");
        log.warn("警告_WARN");
        log.fatal("致命_FATAL");
    }
}
