package com.mj;

import lombok.extern.apachecommons.CommonsLog;

@CommonsLog
public class TestJCL_Log4j1_03 {
    public static void main(String[] args) {
        log.info("信息_INFO");
        log.error("错误_ERROR");
        log.debug("调试_DEBUG");
        log.trace("痕迹_TRACE");
        log.warn("警告_WARN");
        log.fatal("致命_FATAL");
    }
}
