package com.mj.test;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class TestLog4j1_03 {
    public static void main(String[] args) {
        Logger logger = LogManager.getLogger(TestLog4j1_03.class);
        logger.info("信息_INFO");
        logger.error("错误_ERROR");
        logger.debug("调试_DEBUG");
        logger.trace("痕迹_TRACE");
        logger.warn("警告_WARN");
        logger.fatal("致命_FATAL");
    }
}
