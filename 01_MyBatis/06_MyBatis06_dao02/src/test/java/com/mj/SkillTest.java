package com.mj;

import com.mj.bean.Skill;
import com.mj.dao.SkillDao;
import com.mj.util.MyBatises;
import org.apache.ibatis.session.SqlSession;
import org.junit.Assert;
import org.junit.Test;

public class SkillTest {

    @Test
    public void get() {
        try (SqlSession session = MyBatises.openSession()) {
            // 生成SkillDao的代理对象
            SkillDao dao = session.getMapper(SkillDao.class);
            System.out.println(dao.get(1));
        }
    }

    @Test
    public void list() {
        try (SqlSession session = MyBatises.openSession()) {
            // 生成SkillDao的代理对象
            SkillDao dao = session.getMapper(SkillDao.class);
            System.out.println(dao.list());
        }
    }

    @Test
    public void save() {
        try (SqlSession session = MyBatises.openSession(true)) {
            // 生成SkillDao的代理对象
            SkillDao dao = session.getMapper(SkillDao.class);
            Assert.assertTrue(dao.save(new Skill("mj888", 100)));
        }
    }

    @Test
    public void update() {
        try (SqlSession session = MyBatises.openSession(true)) {
            // 生成SkillDao的代理对象
            SkillDao dao = session.getMapper(SkillDao.class);
            Skill skill = new Skill("666", 99);
            skill.setId(80);
            Assert.assertTrue(dao.update(skill));
        }
    }

    @Test
    public void remove() {
        try (SqlSession session = MyBatises.openSession(true)) {
            // 生成SkillDao的代理对象
            SkillDao dao = session.getMapper(SkillDao.class);
            Assert.assertTrue(dao.remove(78));
        }
    }
}
