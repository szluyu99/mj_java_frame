package com.mj;

import com.mj.bean.Person;
import com.mj.dao.PersonDao;
import com.mj.util.MyBatises;
import org.apache.ibatis.session.SqlSession;
import org.junit.Test;

public class PersonTest {
    @Test
    public void get() {
        try (SqlSession session = MyBatises.openSession()) {
            PersonDao dao = session.getMapper(PersonDao.class);
            Person person = dao.get(1);
        }
    }

    @Test
    public void testGet() {
        try (SqlSession session = MyBatises.openSession()) {
            PersonDao dao = session.getMapper(PersonDao.class);
            Person person = dao.testGet();
        }
    }

    @Test
    public void list() {
        try (SqlSession session = MyBatises.openSession()) {
            PersonDao dao = session.getMapper(PersonDao.class);
            System.out.println(dao.list());
        }
    }
}
