package com.mj.service.impl;

import com.mj.dao.SkillDao;
import com.mj.domain.Skill;
import com.mj.service.SkillService;

import java.util.List;

public class SkillServiceImpl implements SkillService {
    private SkillDao dao;

    public void setDao(SkillDao dao) {
        this.dao = dao;
    }

    @Override
    public List<Skill> list() {
        return dao.list();
    }

    @Override
    public boolean save(Skill skill) {
        return dao.save(skill);
    }

    @Override
    public boolean update(Skill skill) {
        return dao.update(skill);
    }

    public void test(Skill skill) throws Exception {

        dao.save(skill);

        throw new Exception();

        // 当前的事务情况
//        update(null);
//
//        save(null);

//        Skill skill = new Skill("777", 777);
//        skill.setId(1);
//        dao.update(skill);
//
//        dao.save(new Skill("888", 888));
    }
}
