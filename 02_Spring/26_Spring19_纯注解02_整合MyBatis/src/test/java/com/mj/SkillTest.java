package com.mj;

import com.mj.dao.SkillDao;
import com.mj.domain.Skill;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;

public class SkillTest {
    private ApplicationContext ctx;
    private SkillDao skillDao;

    @Before
    public void before() {
        ctx = new AnnotationConfigApplicationContext("com.mj");
        skillDao = ctx.getBean("skillDao", SkillDao.class);
    }

    @Test
    public void list() {
        List<Skill> skills = skillDao.list();
        System.out.println(skills);
    }

    @Test
    public void save() {
        System.out.println(skillDao.save(new Skill("123", 456)));
    }
}
