package com.mj;

import com.mj.domain.Skill;
import com.mj.service.SkillService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.List;

public class SkillTest {
    private ApplicationContext ctx;
    private SkillService skillService;

    @Before
    public void before() {
        ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        skillService = ctx.getBean("skillService", SkillService.class);
    }

    @Test
    public void test() throws Exception {
        skillService.test(new Skill("1111", 1111));
    }
}
