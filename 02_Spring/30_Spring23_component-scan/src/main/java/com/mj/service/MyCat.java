package com.mj.service;

import com.mj.domain.Dog;
import com.mj.domain.Person;
import com.mj.filter.MyTypeFilter;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.FilterType;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
//@ComponentScan(basePackages = "com.mj", excludeFilters = {
//        @Filter(type = FilterType.ASSIGNABLE_TYPE, value = {Person.class, Dog.class}),
//        @Filter(type = FilterType.ANNOTATION, classes = Service.class),
//        @Filter(type = FilterType.CUSTOM, classes = MyTypeFilter.class),
//        @Filter(type = FilterType.REGEX, pattern = ".*My.*"),
//        @Filter(type = FilterType.ASPECTJ, pattern = "com.mj.service..*")
//})
public class MyCat {
}
