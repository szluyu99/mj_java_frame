package com.mj.service.impl;

import com.mj.service.UserService;
import org.springframework.stereotype.Service;

@Service("userService")
public class UserServiceImpl implements UserService {
    public void list() {
        System.out.println("UserServiceImpl - list");
    }
}
