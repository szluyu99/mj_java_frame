package com.mj.domain;

import org.springframework.beans.factory.annotation.Autowired;

public class Person {
    private Dog dog;

    public void setDog(Dog dog) {
        this.dog = dog;
    }

    @Override
    public String toString() {
        return "Person{" +
                "dog=" + dog +
                '}';
    }
}
