package com.mj.service;

import com.mj.domain.Skill;

import java.util.List;

public interface SkillService {
    List<Skill> list();

    boolean save(Skill skill);

    void test(Skill skill) throws Exception;
}
