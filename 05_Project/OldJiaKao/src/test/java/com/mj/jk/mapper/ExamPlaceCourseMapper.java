package com.mj.jk.mapper;

import com.mj.jk.pojo.po.ExamPlaceCourse;
import java.util.List;

public interface ExamPlaceCourseMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ExamPlaceCourse record);

    ExamPlaceCourse selectByPrimaryKey(Long id);

    List<ExamPlaceCourse> selectAll();

    int updateByPrimaryKey(ExamPlaceCourse record);
}