package com.mj.jk.mapper;

import com.mj.jk.pojo.po.Coach;
import java.util.List;

public interface CoachMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Coach record);

    Coach selectByPrimaryKey(Long id);

    List<Coach> selectAll();

    int updateByPrimaryKey(Coach record);
}