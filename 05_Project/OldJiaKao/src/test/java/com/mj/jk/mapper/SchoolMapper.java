package com.mj.jk.mapper;

import com.mj.jk.pojo.po.School;
import java.util.List;

public interface SchoolMapper {
    int deleteByPrimaryKey(Long id);

    int insert(School record);

    School selectByPrimaryKey(Long id);

    List<School> selectAll();

    int updateByPrimaryKey(School record);
}