package com.mj.jk.mapper;

import com.mj.jk.pojo.po.ExamPlaceCourseOrder;
import java.util.List;

public interface ExamPlaceCourseOrderMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ExamPlaceCourseOrder record);

    ExamPlaceCourseOrder selectByPrimaryKey(Long id);

    List<ExamPlaceCourseOrder> selectAll();

    int updateByPrimaryKey(ExamPlaceCourseOrder record);
}