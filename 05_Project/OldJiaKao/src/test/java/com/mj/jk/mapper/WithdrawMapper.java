package com.mj.jk.mapper;

import com.mj.jk.pojo.po.Withdraw;
import java.util.List;

public interface WithdrawMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Withdraw record);

    Withdraw selectByPrimaryKey(Long id);

    List<Withdraw> selectAll();

    int updateByPrimaryKey(Withdraw record);
}