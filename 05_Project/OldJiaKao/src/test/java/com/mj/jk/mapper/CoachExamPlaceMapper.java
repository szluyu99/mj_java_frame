package com.mj.jk.mapper;

import com.mj.jk.pojo.po.CoachExamPlace;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CoachExamPlaceMapper {
    int deleteByPrimaryKey(@Param("coachId") Long coachId, @Param("placeId") Long placeId);

    int insert(CoachExamPlace record);

    List<CoachExamPlace> selectAll();
}