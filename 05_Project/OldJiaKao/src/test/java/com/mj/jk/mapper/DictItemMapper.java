package com.mj.jk.mapper;

import com.mj.jk.pojo.po.DictItem;
import java.util.List;

public interface DictItemMapper {
    int deleteByPrimaryKey(Long id);

    int insert(DictItem record);

    DictItem selectByPrimaryKey(Long id);

    List<DictItem> selectAll();

    int updateByPrimaryKey(DictItem record);
}