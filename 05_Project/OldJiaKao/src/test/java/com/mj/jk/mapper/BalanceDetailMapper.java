package com.mj.jk.mapper;

import com.mj.jk.pojo.po.BalanceDetail;
import java.util.List;

public interface BalanceDetailMapper {
    int deleteByPrimaryKey(Long id);

    int insert(BalanceDetail record);

    BalanceDetail selectByPrimaryKey(Long id);

    List<BalanceDetail> selectAll();

    int updateByPrimaryKey(BalanceDetail record);
}