package com.mj.jk.mapper;

import com.mj.jk.pojo.po.Student;
import java.util.List;

public interface StudentMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Student record);

    List<Student> selectAll();
}