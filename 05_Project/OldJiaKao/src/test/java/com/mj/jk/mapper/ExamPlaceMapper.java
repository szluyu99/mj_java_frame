package com.mj.jk.mapper;

import com.mj.jk.pojo.po.ExamPlace;
import java.util.List;

public interface ExamPlaceMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ExamPlace record);

    ExamPlace selectByPrimaryKey(Integer id);

    List<ExamPlace> selectAll();

    int updateByPrimaryKey(ExamPlace record);
}