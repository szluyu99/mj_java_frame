package com.mj.jk.mapper;

import com.mj.jk.pojo.po.AuthLogin;
import java.util.List;

public interface AuthLoginMapper {
    int deleteByPrimaryKey(Long id);

    int insert(AuthLogin record);

    AuthLogin selectByPrimaryKey(Long id);

    List<AuthLogin> selectAll();

    int updateByPrimaryKey(AuthLogin record);
}