package com.mj.jk.mapper;

import com.mj.jk.pojo.po.PlateRegion;
import java.util.List;

public interface PlateRegionMapper {
    int deleteByPrimaryKey(Long id);

    int insert(PlateRegion record);

    PlateRegion selectByPrimaryKey(Long id);

    List<PlateRegion> selectAll();

    int updateByPrimaryKey(PlateRegion record);
}