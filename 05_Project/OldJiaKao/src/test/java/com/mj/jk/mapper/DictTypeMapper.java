package com.mj.jk.mapper;

import com.mj.jk.pojo.po.DictType;
import java.util.List;

public interface DictTypeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(DictType record);

    DictType selectByPrimaryKey(Long id);

    List<DictType> selectAll();

    int updateByPrimaryKey(DictType record);
}