package com.mj.jk.test;

import com.mj.jk.JiaKaoApplication;
import com.mj.jk.service.DictTypeService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = JiaKaoApplication.class)
public class MPTest {
    @Autowired
    private DictTypeService service;
//    @Autowired
//    private DictTypeMapper mapper;

    @Test
    public void testService() {
        System.out.println(service.list());
    }
}
