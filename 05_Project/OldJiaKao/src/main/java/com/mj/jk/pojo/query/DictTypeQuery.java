package com.mj.jk.pojo.query;

import com.mj.jk.pojo.po.DictType;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class DictTypeQuery extends KeywordQuery<DictType> {

}
