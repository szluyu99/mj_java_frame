package com.mj.jk.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mj.jk.pojo.po.DictType;
import com.mj.jk.pojo.query.DictTypeQuery;

public interface DictTypeService extends IService<DictType> {
    void list(DictTypeQuery query);
}
