package com.mj.jk.pojo.query;

import com.mj.jk.pojo.po.DictItem;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class DictItemQuery extends KeywordQuery<DictItem> {

}
