package com.mj.jk.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.mj.jk.pojo.po.DictItem;
import com.mj.jk.pojo.query.DictItemQuery;

public interface DictItemService extends IService<DictItem> {
    void list(DictItemQuery query);
}
